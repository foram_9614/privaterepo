package com.java.hotel;
import java.util.Scanner;

public class HotelMain {
	public static void main(String[] args) {
		System.out.println("Enter the number of hotels");
		Scanner scan =new Scanner(System.in);
		int num= scan.nextInt();
		Hotel hotels[] =new Hotel[num];
		for(int i=0; i<hotels.length; i++){
			hotels[i] = new Hotel();
			System.out.println("Enter name for hotel "+i);
			hotels[i].setName(scan.next());
			System.out.println("Enter location for hotel "+i);
			hotels[i].setLocation(scan.next());
			System.out.println("Enter cuisine for hotel "+i);
			hotels[i].setCusines(scan.next());
			System.out.println("Enter rating for hotel "+i);
			hotels[i].setRating(scan.nextInt());
		}
		
		for(int i=0; i<hotels.length; i++){
			System.out.println(hotels[i]);
		}
		System.out.println("Enter the name of location");
		String loc = scan.next();
		boolean flag = false;
		for(int i=0; i<hotels.length; i++){
			if(hotels[i].getLocation().equals(loc)){
				System.out.println(hotels[i].getName());
				flag = true;
				break;
			}
		}
		if(!flag) System.out.println("Not found in db");
		scan.close();
	}
}
