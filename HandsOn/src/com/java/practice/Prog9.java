package com.java.practice;
import java.util.Scanner;

public class Prog9 {

	public static void main(String[] args) {
		String names[] = {"Ram", "Shyam", "John", "Harry", "Styles"};
		Scanner scan = new Scanner(System.in);
		String name = scan.next();
		int flag =0;
		for(String variable: names){
			if(variable.equals(name)){
				flag=1;
				System.out.println("Name found");
				break;
			}
		}
		if(flag!=1) System.out.println("Name not found");
		scan.close();
		
	} 

}
