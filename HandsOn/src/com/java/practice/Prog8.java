package com.java.practice;
import java.util.*;

public class Prog8 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the day");
		String Day = scan.next();
		switch(Day.toUpperCase()){
		case "MONDAY": case "TUESDAY": case "WEDNESDAY": case "THURSDAY": case "FRIDAY":
			System.out.println("Weekday");
			break;
		case "Saturday": case "SUNDAY":
			System.out.println("Weekend");
			break;
		default: 
			System.out.println("Enter proper day");
		}
		scan.close();
	}

}
