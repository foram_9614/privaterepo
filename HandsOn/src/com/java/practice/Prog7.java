package com.java.practice;
import java.util.Scanner;

public class Prog7 {

	public static void main(String[] args) {
		int arr[]= new int[4];
		Scanner scan =new Scanner(System.in);
		for(int i=0; i<arr.length; i++){
			arr[i] = scan.nextInt();
		}
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
		
		int temp = arr[0];
		for(int i=1; i<arr.length;i++){
			if(temp<arr[i]){
				temp = arr[i];
			}
		}
		System.out.println("Greatest number is "+temp);
		scan.close();
	}

}
