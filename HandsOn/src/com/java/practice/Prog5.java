package com.java.practice;

import java.util.Scanner;

public class Prog5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter three numbers");
		int number1 = scan.nextInt();
		int number2 = scan.nextInt();
		int number3 = scan.nextInt();
		int sum  = number1+number2+number3;
		System.out.println("Sum is "+sum);
		System.out.println("Average is "+(sum/3));
		if(sum > 250){
			System.out.println("A grade");
		}else if(sum >200){
			System.out.println("B grade");
		}else if(sum >150){
			System.out.println("C grade");
		}else if(sum>100){
			System.out.println("D grade");
		}else{
			System.out.println("Fail");
		}
		scan.close();
	}

}
