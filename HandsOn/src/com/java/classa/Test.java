package com.java.classa;
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your name");
		String name = scan.nextLine();
		Employee employee = new Employee();
		System.out.println(employee.greet(name));
		scan.close();
	}
}
