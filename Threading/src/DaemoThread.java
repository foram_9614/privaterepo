
public class DaemoThread extends Thread {

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		for(int i =0; i<10; i++){
			System.out.println("Welcome "+name+ " " +i);
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	} 
	
	public static void main(String[] args) {
		DaemoThread ch = new DaemoThread();
		ch.setName("Monster");
		ch.setDaemon(true);
		ch.setPriority(8);
		ch.start();
	}

}
