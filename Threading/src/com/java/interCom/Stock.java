package com.java.interCom;

public class Stock {
	int val;
	boolean flag;
	public synchronized void setVal(int v){
		System.out.println("Producer ");
		if(flag){
			try{
				wait();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		val =v;
		System.out.println("Put: "+val);
		notify();
		flag = true;
	}
	
	public synchronized int getVal(){
		System.out.println("Consumer");
		if(!flag){
			try{
				wait();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		System.out.println("Got "+val);
		notify();
		flag=false;
		return val;
	}

}
