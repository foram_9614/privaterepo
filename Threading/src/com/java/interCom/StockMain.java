package com.java.interCom;

public class StockMain {
	public static void main(String[] args) {
		Stock st = new Stock();
		
		Consumer consumer = new Consumer("Consumer", st);
		Producer producer = new Producer("Producer", st);
	}

}
