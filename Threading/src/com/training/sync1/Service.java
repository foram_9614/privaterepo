package com.training.sync1;

public class Service implements Runnable {
	String name;
	int tickets;
	Thread thread;
	Booking bookobj;
	

	public Service(String name, Booking bookobj, int tickets) {
		super();
		this.name = name;
		this.tickets = tickets;
		this.bookobj = bookobj;
		thread = new Thread(this, name);
		System.out.println(thread);
		thread.start();
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		synchronized(bookobj){
			System.out.println("Welcome  "+name);
			int amount =bookobj.bookTicket(tickets);
			System.out.println("Charge is "+amount);
		}
		
	}

}
