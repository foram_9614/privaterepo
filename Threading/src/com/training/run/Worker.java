package com.training.run;

public class Worker implements Runnable {
	
		Loan loanobj;
		String name;
		double amount;
		Thread thread;
		
		public Worker( Loan loanobj, String name, double amount) {
			// TODO Auto-generated constructor stub
			super();
			this.loanobj =loanobj;
			this.name = name;
			this.amount = amount;
			thread = new Thread(this, name);
			System.out.println(thread);
			thread.start();
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			synchronized(loanobj){
				System.out.println(thread.getName() + "reached ");
				double interest  = loanobj.calculateInterest(name,amount);
				System.out.println("interest "+interest);
			}
			
		}
		
}
