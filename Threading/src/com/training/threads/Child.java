package com.training.threads;

public class Child extends Thread {

	public Child(String name, int maxPriority) {
		// TODO Auto-generated constructor stub
		super(name);
		this.setPriority(maxPriority);
		System.out.println(this);
		start();
	}

	@Override
	public void run() {
		System.out.println("In run method");
		Thread t = Thread.currentThread();
		String name = t.getName();
		for(int i=0; i<5; i++){
			System.out.println(name +i);
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		System.out.println("Run completed");
	}

	public static void main(String[] args) {
		System.out.println("In main method");
		Child child1 = new Child("Child-1",Thread.MAX_PRIORITY);
		Child child2 = new Child("Child-1",Thread.MAX_PRIORITY);
		
		Thread t = Thread.currentThread();
		String name= t.getName();
		for(int i=0; i<5; i++){
			System.out.println(name +i);
			try{
				Thread.sleep(100);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
		try{
			child1.join();
			child2.join();
		}catch(InterruptedException e){
			e.printStackTrace();
			
		}
		System.out.println("main completed");
	}
	
}
