package com.exception.custom;

public class CusBank {

	int balance =4000;
	void withdraw(int x) throws ODException, NegException, Exception{
		try{
			System.out.println("in Bank ");
			int temp = balance-x;
			if(temp<=0){
				throw new NegException("Exceeded..");
			}else if(x>1000){
				throw new ODException("Allowed upto 1000 RS only");
			}else
				System.out.println("Balance "+balance);
			}catch(Exception e){
				System.out.println("error occured "+e);
				throw e;
			}finally{
				System.out.println("close db");
			}
			System.out.println("Work done");
		
	}

}
