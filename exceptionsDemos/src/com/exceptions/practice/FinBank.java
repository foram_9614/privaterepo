package com.exceptions.practice;

public class FinBank {
	
	void withdraw(int x) throws Exception
	{
		try{
			System.out.println("in bank");
			if(x == 0){
				System.out.println("less than zero");
				throw new Exception();
			}else{
				@SuppressWarnings("unused")
				int bal = 10/x;
				System.out.println("deduct balance");
			}
		}catch(Exception e){
			System.out.println("error occured");
			throw e;
		}finally{
			System.out.println("close db");
		}
		System.out.println("Work Done");
	}
}
