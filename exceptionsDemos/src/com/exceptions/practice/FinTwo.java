package com.exceptions.practice;

public class FinTwo {
	public static void main(String[] args) {
		System.out.println("Welcome");
		
		String val = args[0];
		System.out.println("value got");
		int num =Integer.parseInt(val);
		System.out.println("Converted");
		
		FinBank bank = new FinBank();
		try {
			bank.withdraw(num);
			System.out.println("Amount withdrawn");
		} catch (Exception e) {
			System.out.println("Not withdrawn");
		}
		
		System.out.println("Main completed");
		
	}

}
