package com.project.exception;

public class Validate {
	 
	String arr[] ={"Ram", "Shyam", "John", "Foram","Raj"}; 
	
	boolean validateName(String name) throws NoUniqueNameException{
		for(String variable: arr){
			if(variable.equals(name)){
				throw new NoUniqueNameException("Not a unique name");
			}
		}return true;
	}
	
	boolean checkPassword(String password) throws TooLongException, TooShortException{
		if(password.length()<4){
			throw new TooShortException(" Password is too short");
		}else if(password.length()>9){
			throw new TooLongException(" Password is too long");
		}
		return true;
	}

}