package com.training.client;
import com.training.bean.Hotel;
import java.util.Scanner;
import com.training.service.*;

public class HotelMain {
	public static void main(String[] args) {
		HotelServiceImpl hotelService = new HotelServiceImpl();
		Scanner scan = new Scanner(System.in);
		
		for(int i=0; i<2; i++){
		Hotel hotel = new Hotel();
		System.out.println("Enter the name of hotel");
		hotel.setName(scan.next());
		System.out.println("Enter the location of hotel");
		hotel.setLocation(scan.next());
		System.out.println("Enter the cuisne of hotel");
		hotel.setCusines(scan.next());
		System.out.println("Enter the rating of hotel");
		hotel.setRating(scan.nextInt());
		hotelService.addHotel(hotel);
		
		}

		Hotel[] hotelTemp = new Hotel[5];
		hotelTemp = hotelService.getAllHotels();
		for(Hotel v:hotelTemp){
			System.out.println(v);
		}
		System.out.println("Enter the location");
		String loc = scan.next();
		try{
			hotelTemp = hotelService.getHotelByLocation(loc);
			for(Hotel v:hotelTemp){
				if(v!=null)
				System.out.println(v);
			}
			System.out.println("Enter the rating");
			int rate = scan.nextInt();
			try{
				hotelTemp = hotelService.getHotelByRating(rate);
				for(Hotel v:hotelTemp){
					if(v!=null)
					System.out.println(v);
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		scan.close();
	}

}
