package com.training.service;

import com.training.bean.Hotel;
import com.training.exceptions.HotelNotFoundException;

public class HotelServiceImpl implements HotelService {
	Hotel[] hotels =new Hotel[2];

	int counter=0;
	@Override
	public void addHotel(Hotel hotel) {
		// TODO Auto-generated method stub
		hotels[counter] = hotel;
		counter = counter+1;
		
	}

	@Override
	public Hotel[] getAllHotels() {
		// TODO Auto-generated method stub
		return hotels;
	}

	@Override
	public Hotel[] getHotelByLocation(String loaction) throws HotelNotFoundException {
		// TODO Auto-generated method stub
		Hotel[] temp = new Hotel[2];
		int k=0;
		boolean flag = true;
		for(int i=0; i<hotels.length; i++){
			if(hotels[i].getLocation().equals(loaction)){
				temp[k++] = hotels[i];
				flag=false;
			}
		}
		if(flag) throw new HotelNotFoundException("Hotel name is not there");
		return temp;
	}

	@Override
	public Hotel[] getHotelByRating(int rating) throws HotelNotFoundException {
		// TODO Auto-generated method stub
		Hotel[] temp = new Hotel[2];
		int k=0;
		boolean flag = true;
		for(int i=0; i<hotels.length; i++){
			if(hotels[i].getRating()==(rating)){
				temp[k++] = hotels[i];
				flag=false;
			}
		}
		if(flag) throw new HotelNotFoundException("Hotel name is not there");
		return temp;
	}

}
