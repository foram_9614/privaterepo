package com.training.service;
import com.training.bean.Hotel;
import com.training.exceptions.HotelNotFoundException;

public interface HotelService {
	void addHotel(Hotel hotel);
	Hotel[] getAllHotels();
	Hotel[] getHotelByLocation(String loaction) throws HotelNotFoundException;
	Hotel[] getHotelByRating(int rating) throws HotelNotFoundException;
}
