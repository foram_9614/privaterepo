package com.training.bean;

public class Hotel {
	private String name;
	private String location;
	private String cusines;
	private int rating;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCusines() {
		return cusines;
	}

	public void setCusines(String cusines) {
		this.cusines = cusines;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "Hotel [name=" + name + ", location=" + location + ", cusines=" + cusines + ", rating=" + rating + "]";
	}
	 
}
