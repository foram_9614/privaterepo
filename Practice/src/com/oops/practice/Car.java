package com.oops.practice;

public class Car extends Vehicle{
	
	String type1;

	public Car(String name, String brand, double price, String type1) {
		super(name, brand, price);
		this.type1 = type1;
	}

	@Override
	long calculateMileage() {
		// TODO Auto-generated method stub
		return 50;
	}

	@Override
	void printInsuranceDetails() {
		// TODO Auto-generated method stub
		System.out.println("The insurance is for 3 years");
		
	}
	
	void printAccessories(){
		if (type1.equals("SUV")){
			System.out.println("The SUV has an AC");
		}else {
			System.out.println("The Car doesnot have an AC");
		}
	}
	

}
