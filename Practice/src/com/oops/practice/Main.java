package com.oops.practice;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		Vehicle vehicle = new Bike("Ram", "Honda", 45320, true);
		vehicle.getDetails();
		System.out.println("The milegae of vehicle is "+vehicle.calculateMileage());
		vehicle.printInsuranceDetails();
		Bike  bike = (Bike)vehicle;
		System.out.println("the start type is "+bike.startType());
		
		vehicle = new Car("John", "Hundai", 112212,"SUV");
		vehicle.getDetails();
		System.out.println("The milegae of vehicle is "+vehicle.calculateMileage());
		vehicle.printInsuranceDetails();
		Car car =(Car)vehicle;
		car.printAccessories();
		sc.close();
		
	}

}
