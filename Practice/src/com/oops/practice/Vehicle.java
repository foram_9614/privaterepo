package com.oops.practice;

public abstract class Vehicle {
	
	String name;
	String brand;
	double price;
	
	Vehicle(String name, String brand, double price){
		this.name = name;
		this.brand = brand;
		this.price = price;
	}
	
	void getDetails(){
		System.out.println("The name of the vehicle is "+name);
		System.out.println("The brans id "+brand);
		System.out.println("The price of vehicle is "+price);
		
	}
	
	abstract long calculateMileage();
	
	abstract void printInsuranceDetails();

}
