package com.oops.practice;

public class Bike extends Vehicle{
	boolean autostart;

	public Bike(String name, String brand, double price, boolean autostart) {
		super(name, brand, price);
		this.autostart = autostart;
	}

	@Override
	void getDetails() {
		// TODO Auto-generated method stub
		super.getDetails();
		System.out.println("The autostart is set to "+autostart);
	}

	@Override
	long calculateMileage() {
		// TODO Auto-generated method stub
		return 450;
	}

	@Override
	void printInsuranceDetails() {
		// TODO Auto-generated method stub
		System.out.println("The insurance for bike is for 2 years");
		
	}
	
	String startType(){
		if(autostart){
			return "autostart";
		}else{
			return "notAutostart";
		}
	}
	

}
