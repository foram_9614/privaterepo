package com.exception.practice;
import java.util.Scanner;

class Verification{
	String arr[] = {"Ram","Shyam", "Jack","Vihaan"};
	
	boolean checkName(String name){
		for(int i=0; i<arr.length; i++){
			if(arr[i].equals(name)){
				return true;
				
			}
		}
		return false;
	}
}

public class Checker {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the name of the user");
		String name = scan.nextLine();
		Verification verify = new Verification();
		boolean check = verify.checkName(name);
		if(check){
			System.out.println("Welcome "+name);
		}else{
			System.out.println("Sorry user not found");
		}
		scan.close();
	}
	
	
	

}
