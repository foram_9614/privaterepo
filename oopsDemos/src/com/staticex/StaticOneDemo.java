package com.staticex;

public class StaticOneDemo {
	static int x,y=20;
	static{
		System.out.println("in static block 1");
		x=10;
	}
	static{
		System.out.println("in static block 2");
		x=40; y=200;
	}
	public StaticOneDemo(){
		super();
		System.out.println("in constructor");
	}
	{
		System.out.println("init block 1");
	}
	{
		System.out.println("init block 2");
	}
	static void getMessage(){
		System.out.println("in sattic methos");
	}
}
