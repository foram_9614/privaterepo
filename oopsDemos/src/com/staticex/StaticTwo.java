package com.staticex;

import static java.lang.System.out;
import static com.staticex.StaticOneDemo.getMessage;

public class StaticTwo {
	/*static{
		System.out.println("In sattic block two");
	}*/
	public static void main(String[] args) {
		out.println("in main of static 2");
		getMessage();
		System.out.println("Sum "+(StaticOneDemo.x+StaticOneDemo.y));
		
		int val = Integer.parseInt("100");
		System.out.println("value "+val);
		
		StaticOneDemo sd = new StaticOneDemo();
		sd.getMessage();
	}

}
