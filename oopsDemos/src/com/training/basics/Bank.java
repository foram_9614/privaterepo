package com.training.basics;

public class Bank {
	double balance;
	
	double getBalance(){
		return balance;
	}
	
	void withdraw(int amount){
		if(balance-amount>=0){
			balance = balance-amount;
			System.out.println("Withdrawn "+amount+ " amount of money");
			
		}else{
			System.out.println("Insufficient Balance");
		}
	}
	
	void deposit(int amount){
		balance = balance+amount;
		System.out.println("Amount deposited is "+amount);
	}
}
