package com.training.basics;

public class Employee {
	String name;
	int salary;
	
	public Employee(String name, int salary) {
		super();
		this.name = name;
		this.salary = salary;
	}

	void getDetails(){
		System.out.println("name : " +name);
		System.out.println("salary : " +salary);
		
	}
	
	String greet(String msg){
		return "welcome " +msg;
	}
}
