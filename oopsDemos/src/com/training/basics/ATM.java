package com.training.basics;

public class ATM {
	public static void main(String[] args) {
		Bank b =new Bank();
		b.balance = 5000;
		b.deposit(1000);
		b.withdraw(4000);
		b.withdraw(3000);
		System.out.println("Balance is "+b.getBalance());
	}

}
