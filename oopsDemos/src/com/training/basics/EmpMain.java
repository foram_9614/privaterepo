package com.training.basics;

public class EmpMain {
	public static void main(String[] args) {
		Employee e = new Employee("Ram",10000);
		e.getDetails();
		System.out.println(e.greet("Java"));
		
		Employee e1 = new Employee("John",2000);
		e1.getDetails();
		System.out.println(e1.greet("Java"));
		
		Employee e2;
		e2 = e1;
		e1 = null;
		System.out.println(e2.name+" "+e2.salary);
		e2.getDetails();
		System.out.println(e2.greet("Java"));
		//System.out.println(e1.name+" "+e1.salary);
		
	}

}
