package com.oops.overloading;

public class VehicleMain {
	public static void main(String[] args) {
		Vehicle v = new Vehicle();
		Vehicle v1 = new Vehicle("Ram");
		Vehicle v2 = new Vehicle("John","Hundai");
		Vehicle v3 = new Vehicle("Shyam","dgsj",200000);
		v.getFeatures();
		v1.getFeatures();
		v2.getFeatures();
		v3.getFeatures();
		
	}
	
	

}
