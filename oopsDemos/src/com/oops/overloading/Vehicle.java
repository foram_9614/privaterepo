package com.oops.overloading;

public class Vehicle {
	
	String name;
	String brand;
	int price;
	
	public Vehicle() {
		super();
		// TODO Auto-generated constructor stub
		System.out.println("Vehicle constructor invoked");
	}
	
	public Vehicle(String name) {
		this();
		System.out.println("Name initialised");
		this.name = name;
	}

	
	
	public Vehicle(String name, String brand) {
		this(name);
		System.out.println("Brand initialised");
		this.brand = brand;
	}

	public Vehicle(String name, String brand, int price) {
		this(name,brand);
		System.out.println("Price initialised");
		this.price = price;
	}

	
	void getFeatures(){
		if(name!=null){
			System.out.println("name "+name);
		}if(brand!=null){
			System.out.println("brand "+brand);
		}if(price!=0){
			System.out.println("price "+price);

		}
		
		
	}
	
	

}
