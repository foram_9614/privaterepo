package com.oops.overloading;

public class ThisDemo {
		public ThisDemo(int x){
			this("Ram");
			System.out.println("Value "+x);
		}
		
		public ThisDemo(String name){
			System.out.println("Welcome "+name);			
		}
		
		public ThisDemo(){
			this(10);
			System.out.println("Hello");
		}
		
		public static void main(String[] args) {
			ThisDemo dem = new ThisDemo();
		}
		
}
