package com.oops.overloading;

public class Demo {
	public static void main(String[] args) {
		Shape shape = new Shape();
		shape.calcArea(10);
		shape.calcArea(10.5f);
		
		int rect = shape.calcArea(10, 20);
		System.out.println("Rect "+rect);
		
		double tri = shape.calcArea(10, (int)20.5f);
		System.out.println("tri "+tri);
	}

}
