package com.oops.overloading;

public class Shape {
	void calcArea(int x){
		System.out.println("Sq "+x*x);
	}
	
	int calcArea(int x, int y){
		return x*y;
	}
	
	void calcArea(float x){
		System.out.println("Cir "+3.14*x*x);
	}
	
	double calcArea(int x, float y){
		return 0.5*x*y;
	}
}
