package com.oops.overloading;

public class VarArDemo {
	public static void main(String[] args) {
		VarArDemo s = new VarArDemo();
		s.calcSum("Ram");
		s.calcSum("Tom",90);
		
				
	}
	
	
	void calcSum(String name, int... b){
		System.out.println("Welcome "+name);
		int sum =0;
		for(int v:b)
			sum= sum+v;
		System.out.println("sum "+sum);
	}
	
	void calcSum(String name){
		System.out.println("Have a good day "+name);
	}
}
