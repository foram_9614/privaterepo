package com.oops.interfaces;

public class Main {
	public static void main(String[] args) {
		Inter1 ref;
		ref = new Interclass();
		ref.m1();
		ref.m2();
		
		
		ref = new SubClass();
		
		ref.m1();
		ref.m2();
		SubClass sub= (SubClass)ref;
		sub.m3();
		sub.m4();
	}

}
