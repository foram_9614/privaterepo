package com.oops.encapsulation;

public class Mobile {
	private String brand;
	private double price;
	private String type;
	
	
	public void setBrand(String brand){
		this.brand = brand;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return "Mobile [brand=" + brand + ", price=" + price + ", type=" + type + "]";
	}
}