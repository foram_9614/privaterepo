package com.oops.interface1;

public class Parrot extends Bird implements Flyable, Talkable {

	public Parrot(String name, String color, String beakType, String food) {
		super(name, color, beakType, food);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void talk() {
		// TODO Auto-generated method stub
		System.out.println("The parrot can talk");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("The parrot can fly");
	}

	@Override
	void getHabitat() {
		// TODO Auto-generated method stub
		System.out.println("The habitat of Parrot is tree");
	}
	
	void getSound(){
		System.out.println("The sound of Parrot is quack quack");
	}

}
