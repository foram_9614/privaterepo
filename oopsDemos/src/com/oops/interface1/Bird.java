package com.oops.interface1;

public abstract class Bird {
	private String name;
	private String color;
	private String beakType;
	private String food;
	
	public Bird(String name, String color, String beakType, String food) {
		super();
		this.name = name;
		this.color = color;
		this.beakType = beakType;
		this.food = food;
	}

	void getFeatures(){
		System.out.println("Name is "+name);
		System.out.println("Color is  "+color);
		System.out.println("beakType is "+beakType);
		System.out.println("Food eaten is "+food);
	}
	
	abstract void getHabitat();
}
