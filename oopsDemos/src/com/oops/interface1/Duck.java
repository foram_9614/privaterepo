package com.oops.interface1;

public class Duck extends Bird implements Flyable, Swimmable {

	public Duck(String name, String color, String beakType, String food) {
		super(name, color, beakType, food);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("The Duck is swimming");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("the duck is flying");
	}

	@Override
	void getHabitat() {
		// TODO Auto-generated method stub
		System.out.println("The habitat of duck is water");
	}
	
	void getFeatType(){
		System.out.println("the duck has feathers");
	}

}
