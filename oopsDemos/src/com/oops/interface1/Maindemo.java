package com.oops.interface1;

public class Maindemo {

	public static void main(String[] args) {
		Bird bird = new Parrot("tweety", "Green", "sharp", "chilly");
		bird.getFeatures();
		bird.getHabitat();
		
		Flyable bref = (Flyable)bird;
		bref.fly();
		
		Talkable nref = (Talkable)bird;
		nref.talk();
		
		Parrot parrot = (Parrot)bird;
		parrot.getSound();
		/*parrot.fly();
		parrot.talk();*/
		
		bird = new Duck("Ducky", "white", "Blunt", "worms");
		bird.getFeatures();
		bird.getHabitat();
		
		
		Flyable fly1 = (Flyable)bird;
		fly1.fly();
		
		Swimmable nref1 = (Swimmable)bird;
		nref1.swim();
		
		Duck duck = (Duck)bird;
		duck.getFeatType();
		/*duck.fly();
		duck.swim();*/
	}

}
