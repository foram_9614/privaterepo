package com.oops.abstraction;

public class BankMain {
	public static void main(String[] args) {
		ABank ref = new Branch1();
		ref.carLoan();
		ref.housingLoan();
		ref.admin();
		System.out.println(ref.val);
		
		Branch1 br1 = (Branch1)ref;
		br1.br1Method();
		
		ref = new Subbranch();
		ref.carLoan();
		ref.housingLoan();
		ref.admin();
		
		Branch2 br2= (Branch2)ref;
		br2.br2Method();
		
		
		Subbranch sub= (Subbranch)ref;
		sub.subPay();
		
		
	}

}
