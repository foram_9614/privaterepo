package com.oops.abstraction;

public abstract class ABank {
	int val = 1000;
	abstract void housingLoan();
	abstract void carLoan();
	
	final void admin(){
		System.out.println("In ABank admin");
	}
}
