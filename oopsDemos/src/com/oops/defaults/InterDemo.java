package com.oops.defaults;


interface DefInter1{
	public default void greet(){
		System.out.println("Hello world");
	}
	
	public void caller();
	
}

class MyTest implements DefInter1{

	@Override
	public void greet() {
		//System.out.println("in the implementing class");
		DefInter1.super.greet();
	}

	@Override
	public void caller() {
		System.out.println("Welcome back");
	}
	
}
public class InterDemo {

	public static void main(String[] args) {
		DefInter1 ref = new MyTest();
		ref.greet();
		ref.caller();
		
	}

}
