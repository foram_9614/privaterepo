package com.oops.inheritance;

public class Employee {
	String name,city;
	Employee(String name, String city){
		super();
		this.name = name;
		this.city = city;
	}
	
	public double calcBonus(int x){
		System.out.println("Calculating emp");
		return x;
	}
	
	void greet(){
		System.out.println("Welcome ");
	}
	
	/*void getDetails(){
		System.out.println("In super class");
		System.out.println("name "+name);
		System.out.println("age "+age);
	}*/
}
