package com.oops.inheritance;

public class InherDemo {
	public static void main(String[] args) {
		Employee e = new Employee("Ram","Bangalore");
		System.out.println(e.calcBonus(5));
		e.greet();
		//e.getDetails();
		e = new Manager("asjd","huwe",45);
		System.out.println(e.calcBonus(10));
		Manager man = (Manager)e;
		man.adminDetails();
		
		e = new Programmer("Jack","Bangalore");
		System.out.println(e.calcBonus(10));
		e.greet();
		//ob.getDetails();
		//ob.m2();
	}

}
