package com.oops.inheritance;

public class Manager extends Employee{
	int salary;
	Manager(String name, String city, int salary){
		super(name,city);
		this.salary = 1000;
	}
	

	@Override
	public double calcBonus(int x) {
		// TODO Auto-generated method stub
		return x*5;
	}

	public void adminDetails(){
		System.out.println("in manager");
	}
	void m2(){
		System.out.println("In sub class");
		System.out.println("salary "+salary);
	}
	
}
